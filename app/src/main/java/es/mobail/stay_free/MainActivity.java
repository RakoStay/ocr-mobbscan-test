package es.mobail.stay_free;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mobbeel.mobbscan.api.MobbScanAPI;
import com.mobbeel.mobbscan.api.MobbScanAPIError;
import com.mobbeel.mobbscan.api.MobbScanDocumentSide;
import com.mobbeel.mobbscan.api.MobbScanDocumentType;
import com.mobbeel.mobbscan.api.MobbScanOperationMode;
import com.mobbeel.mobbscan.api.listener.IDDocumentDetectionListener;
import com.mobbeel.mobbscan.api.listener.IDDocumentScanListener;
import com.mobbeel.mobbscan.api.listener.LicenseStatusListener;
import com.mobbeel.mobbscan.api.listener.ScanStartListener;
import com.mobbeel.mobbscan.api.result.MobbScanDetectionResult;
import com.mobbeel.mobbscan.api.result.MobbScanDetectionResultData;
import com.mobbeel.mobbscan.api.result.MobbScanLicenseResult;
import com.mobbeel.mobbscan.api.result.MobbScanScanResult;
import com.mobbeel.mobbscan.api.result.MobbScanScanResultData;
import com.mobbeel.mobbscan.api.result.MobbScanStartScanResult;
import com.mobbeel.mobbscan.document.IDDocument;

import java.util.Date;

public class MainActivity extends Activity implements IDDocumentScanListener, IDDocumentDetectionListener {

    //ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //MobbScanAPI.getInstance().setBaseUrl("https://mobbscan-dev.mobbeel.com/");
        MobbScanAPI.getInstance().setBaseUrl("https://mobbscan-pre.mobbeel.com/");
        MobbScanAPI.getInstance().initAPI("c40af0b1-0aea-4c50-b5b5-a83fe06cab2f", this, new LicenseStatusListener() {
            @Override
            public void onLicenseStatusChecked(MobbScanLicenseResult licenseResult, Date licenseValidTo) {
                if (licenseResult != MobbScanLicenseResult.VALID) {
                    //Toast.makeText(MainActivity.this, "There was a problem with the license", Toast.LENGTH_LONG).show();

                }
            }
        });
        //Progress dialog to show while a scan document is in progress
        //progressDialog = new ProgressDialog(this);
        //progressDialog.setIndeterminate(true);
        // progressDialog.setTitle("MobbScan");
        // progressDialog.setCancelable(false);
    }

    private void scan(final MobbScanOperationMode operationMode) {
        refreshUI(null);
        //progressDialog.setMessage("Starting scan process...");
        //progressDialog.show();

        MobbScanAPI.getInstance().startScan(MobbScanDocumentType.ESPIDCardV3, operationMode, new ScanStartListener() {
            @Override
            public void onScanStarted(MobbScanStartScanResult result, String scanId, MobbScanAPIError error) {
                if (result == MobbScanStartScanResult.OK) {
                    if (operationMode == MobbScanOperationMode.SCAN_ONLY_FRONT) {
                        MobbScanAPI.getInstance().scanDocument(MobbScanDocumentSide.FRONT, scanId, MainActivity.this, MainActivity.this);
                    } else if (operationMode == MobbScanOperationMode.SCAN_ONLY_BACK) {
                        MobbScanAPI.getInstance().scanDocument(MobbScanDocumentSide.BACK, scanId, MainActivity.this, MainActivity.this);
                    } else if (operationMode == MobbScanOperationMode.SCAN_BOTH_SIDES) {
                        MobbScanAPI.getInstance().scanDocument(MobbScanDocumentSide.FRONT, scanId, MainActivity.this, MainActivity.this);
                        MobbScanAPI.getInstance().scanDocument(MobbScanDocumentSide.BACK, scanId, MainActivity.this, MainActivity.this);
                    }
                } else {
                    //progressDialog.hide();
                    Toast.makeText(MainActivity.this, "Error: The scan process could not be started. Please, contact with Mobbeel", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void scanFront(View view) {
        scan(MobbScanOperationMode.SCAN_ONLY_FRONT);
    }

    public void scanBack(View view) {
        scan(MobbScanOperationMode.SCAN_ONLY_BACK);
    }

    public void scanBoth(View view) {
        scan(MobbScanOperationMode.SCAN_BOTH_SIDES);
    }

    private void refreshUI(IDDocument document) {
        if (document != null) {
            ((TextView) findViewById(R.id.tvPeronalNumber)).setText(document.getPersonalNumber());
            ((TextView) findViewById(R.id.tvDocumentNumber)).setText(document.getDocumentNumber());
            ((TextView) findViewById(R.id.tvNameAndSurname)).setText(document.getName() + " " + document.getSurname());
            ((TextView) findViewById(R.id.tvDateOfBirth)).setText(document.getDateOfBirth());
            ((TextView) findViewById(R.id.tvGender)).setText(document.getGender());
            ((TextView) findViewById(R.id.tvValidTo)).setText(document.getDateOfExpiry());
            ((TextView) findViewById(R.id.tvNationality)).setText(document.getNationality());
        } else {
            ((TextView) findViewById(R.id.tvPeronalNumber)).setText("");
            ((TextView) findViewById(R.id.tvDocumentNumber)).setText("");
            ((TextView) findViewById(R.id.tvNameAndSurname)).setText("");
            ((TextView) findViewById(R.id.tvDateOfBirth)).setText("");
            ((TextView) findViewById(R.id.tvGender)).setText("");
            ((TextView) findViewById(R.id.tvValidTo)).setText("");
            ((TextView) findViewById(R.id.tvNationality)).setText("");
        }

    }

    @Override
    public void onIDDocumentScanned(MobbScanScanResult result, MobbScanScanResultData resultData, MobbScanAPIError error) {
        if (result == MobbScanScanResult.COMPLETED) {
            refreshUI(resultData.getIdDocument());
        } else if (result == MobbScanScanResult.ERROR) {
            Toast.makeText(MainActivity.this, "There was an error during scan process = " + error.toString(), Toast.LENGTH_LONG).show();
        }
        //if (result != MobbScanScanResult.PENDING_OTHER_SIDE && progressDialog!=null && progressDialog.isShowing()) {
            //progressDialog.dismiss();
        //}
    }

    @Override
    public void onIDDocumentDetected(MobbScanDetectionResult result, MobbScanDetectionResultData resultData, MobbScanAPIError error) {
        //progressDialog.setMessage("Extracting document information...");
        if (result == MobbScanDetectionResult.OK) {
            // free allocated memory if you don't use the bitmap result
            if (resultData.getImage() != null && !resultData.getImage().isRecycled()) {
                resultData.getImage().recycle();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MobbScanAPI.getInstance().release();
    }

}
